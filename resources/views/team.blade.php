<!DOCTYPE html>
<html lang="en">

<head>
    <title>Chopper-tech - Jasa pembuatan website dan aplikasi murah</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">


    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    @extends('components.nav')
    <section class="hero-wrap hero-wrap-2 ftco-degree-bg js-fullheight" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate pb-5 text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Team <i class="ion-ios-arrow-forward"></i></span></p>
                    <h1 class="mb-3 bread">Team</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-agent">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="agent">
                        <div class="img">
                            <img src="images/team-1.jpg" class="img-fluid" alt="Colorlib Template">
                        </div>
                        <div class="desc">
                            <h3><a href="properties.html">James Stallon</a></h3>
                            <p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="agent">
                        <div class="img">
                            <img src="images/team-2.jpg" class="img-fluid" alt="Colorlib Template">
                        </div>
                        <div class="desc">
                            <h3><a href="properties.html">James Stallon</a></h3>
                            <p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="agent">
                        <div class="img">
                            <img src="images/team-3.jpg" class="img-fluid" alt="Colorlib Template">
                        </div>
                        <div class="desc">
                            <h3><a href="properties.html">James Stallon</a></h3>
                            <p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="agent">
                        <div class="img">
                            <img src="images/team-4.jpg" class="img-fluid" alt="Colorlib Template">
                        </div>
                        <div class="desc">
                            <h3><a href="properties.html">James Stallon</a></h3>
                            <p class="h-info"><span class="position">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="agent">
                        <div class="img">
                            <img src="images/team-5.jpg" class="img-fluid" alt="Colorlib Template">
                        </div>
                        <div class="desc">
                            <h3><a href="properties.html">James Stallon</a></h3>
                            <p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="agent">
                        <div class="img">
                            <img src="images/team-6.jpg" class="img-fluid" alt="Colorlib Template">
                        </div>
                        <div class="desc">
                            <h3><a href="properties.html">James Stallon</a></h3>
                            <p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="agent">
                        <div class="img">
                            <img src="images/team-7.jpg" class="img-fluid" alt="Colorlib Template">
                        </div>
                        <div class="desc">
                            <h3><a href="properties.html">James Stallon</a></h3>
                            <p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="agent">
                        <div class="img">
                            <img src="images/team-8.jpg" class="img-fluid" alt="Colorlib Template">
                        </div>
                        <div class="desc">
                            <h3><a href="properties.html">James Stallon</a></h3>
                            <p class="h-info"><span class="position">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @extends('components.footer')
</body>

</html>